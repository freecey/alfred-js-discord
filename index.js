const { Client, VoiceChannel, Intents, Constants, MessageEmbed } = require("discord.js");
const { createAudioResource, StreamType, createAudioPlayer, joinVoiceChannel, getVoiceConnection } = require('@discordjs/voice');
const { join } = require('path');
const fs = require('fs');
require("dotenv").config();
const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args))
const fileNames = fs.readdirSync('./assets/img/run');

const player = createAudioPlayer();
const PREFIX = process.env.PREFIX

const client = new Client({
    intents: ["GUILDS", "GUILD_MESSAGES", "GUILD_MESSAGE_REACTIONS", "GUILD_VOICE_STATES"]
});

client.on("ready", async () => {
    client.user.setPresence({
        activities: [{
            name: 'attendre: !a help', type: 'PLAYING'
        }], status: 'online'
    });

    console.log('Connected as :' + client.user.tag);
});

var randomInteger = function (pow) {
    return Math.floor(1 + Math.random() * pow);
};

client.on('messageCreate', async message => {
    if (message.content.startsWith(PREFIX)) {
        const input = message.content.slice(PREFIX.length).trim().split(" ")
        const command = input.shift();
        switch (command) {
            case 'help':
                help();
                break;
            case 'joke':
            case 'chuck':
                chuck();
                break;
            case 'batman':
                batmat();
                break;
            case 'de':
                deroll(input[0]);
                break
            case 'reboot':
                resetBot(message.channel);
                break;
            case 'rtxfe':
                rtxfe(input[0]);
                break;
            default:
                defaultmsg();
                break;

        }
    }
    function help() {
        const EmbedHelp = new MessageEmbed()
            .setColor('#0099ff')
            .setTitle('Alfred P. - Discord.js Bot')
            .setURL('https://gitlab.yana.xyz/cedric/alfred-js-discord')
            .setAuthor({ name: 'Alfred P.', iconURL: 'attachment://Alfred.webp', url: 'https://gitlab.yana.xyz/cedric/alfred-js-discord' })
            .setDescription('**Voici la liste des commandes du bot**')
            .setThumbnail('attachment://Alfred.webp')
            .addFields(
                { name: '**help** :', value: 'Affiche l\'aide du bot' },
                { name: '**de** :', value: 'lance un dé pour vous (un dé 10 par défaut, mais vous pouvez en spécifier un autre "!a de xxx").' },
                { name: '**chuck** or **joke** :', value: 'Un petit Chuck Norris Fact.' },
                { name: '**batman** :', value: '???????' },
                { name: '**rtxfe** :', value: 'Recevoir les DROP RTX FR (ajouter remove pour s\'enlever de la liste' },
                { name: '\u200B', value: '\u200B' },
            )
            .setTimestamp()
            .setFooter('by Cedric', 'https://i.pinimg.com/originals/b1/45/92/b145929e78c131593b303a042b6dfa16.jpg');
        message.channel.send({ embeds: [EmbedHelp], files: ['./assets/img/Alfred.webp'] });
        message.delete();
    }

    function defaultmsg() {
        message.reply("Je ne connais pas cette tache, \nQue puis-je faire pour vous.");
        message.delete();
    }

    function chuck() {
        let url = 'https://chuckn.neant.be/api/rand';

        fetch(url).then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Something went wrong');
            }
        })
            .then((responseJson) => {
                const EmbedHelp = new MessageEmbed()
                    .setColor('#ff8d33')
                    .setTitle(responseJson.joke)
                    .setURL('https://chuckn.neant.be/jokes/' + responseJson.id)
                    .setAuthor({ name: 'Alfred P.', iconURL: 'attachment://Alfred.webp', url: 'https://gitlab.yana.xyz/cedric/alfred-js-discord' })
                    .setDescription('Fact #' + responseJson.id)
                message.channel.send({ embeds: [EmbedHelp], files: ['./assets/img/Alfred.webp'] });
                message.delete();
            })
            .catch((error) => {
                console.log(error)
            });
    }

    async function batmat() {
        batImg = fileNames[randomInteger(fileNames.length - 1)];
        const embed = new MessageEmbed().setTitle('Na na na na na na na na na na na na na na na\n... BATMAN!').setImage(`attachment://${batImg}`);
        message.channel.send({ embeds: [embed], files: [`./assets/img/run/${batImg}`] });
        message.delete();
        if (message.member.voice.channel) {
            const channel = message.member?.voice.channel;
            if (channel) {
                try {
                    joinVoiceChannel({
                        channelId: message.member.voice.channel.id,
                        guildId: message.guild.id,
                        adapterCreator: message.guild.voiceAdapterCreator
                    }).subscribe(player)
                    let resource = createAudioResource(join('./assets/audio/Batman_Theme_Song.mp3'));
                    player.play(resource);
                } catch (error) {
                    console.error(error);
                }
            } else {
                message.reply('Join a voice channel then try again!');
            }
        }
    }

    function deroll(nb) {
        if (!isNaN(nb) || typeof nb == "undefined") {
            nb = (nb != null) ? nb : 10;
            var rand = randomInteger(nb);
            msg = "J'ai lance pour vous un dé " + nb + " Vous avez obtenu **" + rand + "**";
        } else {
            msg = "Je n\'ai pas de dé \"" + nb + "\" désolé."
        }
        message.reply(`${msg}`);
        // message.delete();
    }

    function rtxfe(input) {

        if (input != 'remove') {

            let readedfile = fs.readFileSync('./nv_files/id_send').toString();
            let add_id = message.author.id;
            if (readedfile.split('#').includes(add_id)) {
                message.author.send('Deja dans la liste')
            } else {
                readedfile += add_id + '#';
                fs.writeFile('./nv_files/id_send', readedfile, function (err) {
                    if (err) throw err;
                });
                client.users.fetch(add_id).then((user) => {
                    user.send("Ajouter à la liste des drop");
                });
            }

        } else {
            let readedfile = fs.readFileSync('./nv_files/id_send').toString().replace(message.author.id + "#", '');
            fs.writeFile('./nv_files/id_send', readedfile, function (err) {
                if (err) throw err;
            });
            message.author.send('Retirer de la liste des drop')
        }
        message.delete();
    }

});

function resetBot(channel) {
    channel.send('Resetting...')
        .then(msg => client.destroy())
        .then(() => client.login(`${process.env.CLIENT_TOKEN}`));
}

client.login(`${process.env.CLIENT_TOKEN}`);

var headers = {
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "en-US,en;q=0.5",
    "Cache-Control": "max-age=0",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0",
    "Connection": "keep-alive",
}

let url = 'https://api.nvidia.partners/edge/product/search?page=1&limit=9&locale=fr-fr&manufacturer=NVIDIA&gpu=RTX%203060%20Ti,RTX%203070,RTX%203070%20Ti,RTX%203080,RTX%203080%20Ti&gpu_filter=RTX%203090~1,RTX%203080%20Ti~1,RTX%203080~1,RTX%203070%20Ti~1,RTX%203070~1,RTX%203060%20Ti~1,RTX%203060~0,RTX%203050%20Ti~0,RTX%203050~0,RTX%202080%20Ti~0,RTX%202080%20SUPER~0,RTX%202080~0,RTX%202070%20SUPER~0,RTX%202070~0,RTX%202060~0,GTX%201660%20Ti~0,GTX%201660%20SUPER~0,GTX%201660~0,GTX%201650%20Ti~0,GTX%201650%20SUPER~0,GTX%201650~0';

const interval = setInterval(
    function nvcheckapi() {
        fetch(url, { method: 'GET', headers: headers }).then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Something went wrong');
            }
        })
            .then((responseJson) => {
                responseJson.searchedProducts.productDetails.forEach(product => {
                    let readedfile = fs.readFileSync('./nv_files/' + product.productSKU + '.txt').toString();
                    NVCard = `${JSON.stringify(product, null, 4)}`;
                    if (readedfile !== NVCard) {
                        fs.writeFile('./nv_files/' + product.productSKU + '.txt', NVCard, function (err) {
                            if (err) throw err;
                            console.log('Saved! - change on ' + product.productTitle);
                            let usersid = fs.readFileSync('./nv_files/id_send').toString().split('#');

                            const EmbedHelp = new MessageEmbed()
                                .setColor('#0099ff')
                                .setTitle(product.productTitle)
                                .setURL((product.retailers[0].purchaseLink != null ? product.retailers[0].purchaseLink.toString() : 'https://store.nvidia.com/fr-fr/geforce/store/?page=1&limit=9&locale=fr-fr&manufacturer=NVIDIA&manufacturer_filter=NVIDIA~7,ACER~10,ALIENWARE~3,AORUS~6,ASUS~52,DELL~6,EVGA~5,GAINWARD~1,GIGABYTE~45,HP~17,INNO3D~2,LENOVO~3,MSI~108,PALIT~4,PNY~4,RAZER~17,ZOTAC~13'))
                                .setAuthor({ name: 'Alfred P. - DROP RTX FE', iconURL: 'attachment://Alfred.webp', url: 'https://gitlab.yana.xyz/cedric/alfred-js-discord' })
                                .setDescription('DROP RTX FE on LDLC')
                                .setThumbnail(product.imageURL)
                                .addFields(
                                    { name: 'Prix :', value: product.productPrice.toString() },
                                    { name: 'prdStatus :', value: product.prdStatus.toString() },
                                    { name: 'purchaseLink :', value: (product.retailers[0].purchaseLink != null ? product.retailers[0].purchaseLink.toString() : 'NULL') },
                                    { name: 'directPurchaseLink :', value: (product.retailers[0].directPurchaseLink != null ? product.retailers[0].directPurchaseLink.toString() : 'NULL') },
                                    { name: 'stock', value: product.retailers[0].stock.toString() },
                                    { name: 'hasOffer', value: product.retailers[0].hasOffer.toString() },
                                    { name: 'salePrice', value: product.retailers[0].salePrice.toString() },
                                )
                                .setTimestamp()

                            usersid = usersid.filter((a) => a);
                            usersid.forEach(user_id =>
                                client.users.fetch(user_id).then((user) => {
                                    user.send({ embeds: [EmbedHelp], files: ['./assets/img/Alfred.webp'] });
                                })
                            );
                        });

                    }
                });
                return responseJson;
            })
            .catch((error) => {
                let usersid = fs.readFileSync('./nv_files/id_send').toString().split('#');
                usersid = usersid.filter((a) => a);
                usersid.forEach(user_id =>
                    client.users.fetch(user_id).then((user) => {
                        user.send('Error on nvidia API - '+ error);
                    })
                );
                console.log(error);
                
            });
    }, 10000);

    // TODO : CHANGE setInterval if error api !!!!